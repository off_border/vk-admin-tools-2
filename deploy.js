/*global process */

const fs = require('fs');
const credentials = require('./deploy.config'); // You need to create this file, and put there your credentials

let zipName = './dist/build/build.zip';

// credentials and IDs from gitlab-ci.yml file (your appropriate config file)
let REFRESH_TOKEN = process.env.REFRESH_TOKEN  || credentials.refresh_token;
let EXTENSION_ID = process.env.EXTENSION_ID  || credentials.extension_id;
let CLIENT_SECRET = process.env.CLIENT_SECRET  || credentials.client_secret;
let CLIENT_ID = process.env.CLIENT_ID  || credentials.client_id;

const webStore = require('chrome-webstore-upload')({
    extensionId: EXTENSION_ID,
    clientId: CLIENT_ID,
    clientSecret: CLIENT_SECRET,
    refreshToken: REFRESH_TOKEN
});

function uploadZip(zipPath) {
    // creating file stream to upload
    const extensionSource = fs.createReadStream(zipPath);

    // upload the zip to webstore
    webStore.uploadExisting(extensionSource).then(() => {
        console.log('Successfully uploaded the ZIP');

        // publish the uploaded zip
        webStore.publish().then(() => {
            console.log('Successfully published the newer version');
        }).catch((error) => {
            console.log(`Error while publishing uploaded extension: ${error}`);
            process.exit(1);
        });

    }).catch((error) => {
        console.log(`Error while uploading ZIP: ${error}`);
        process.exit(1);
    });
}

uploadZip(zipName);