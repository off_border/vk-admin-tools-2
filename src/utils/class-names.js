// module.exports = class ClassNames {
//     constructor(baseName) {
//         this._baseName = baseName;
//         this._mods = [];
//     }

//     getClassObject() {
//         let classList = [this._baseName];

//         let modsList = this._mods.map(mod => {
//             return mod;
//         });

//         return classList.concat(modsList);
//     }
// }

function getClassObject({ name, mods = {} }) {
    const modsObj = Object.keys(mods)
        .reduce((result, modName) => {
            const modVal = mods[modName];

            if (modVal) {
                const modEnding = modVal !== true ? 
                    '_' + modVal :
                    '';
        
                result[name + '_' + modName + modEnding] = modVal;
            }

            return result;
        }, {});

    return {
        [name]: true,
        ...modsObj
    };
}

module.exports = function getClassBuilder(baseName) {
    let builder = function(mods) {
        return getClassObject({
            name: baseName,
            mods
        });
    };

    builder.elem = function(elemName) {
        return getClassBuilder(baseName + '__' + elemName);
    };

    return builder;
};