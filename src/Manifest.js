module.exports = ({ mode }) => {
    const isDevMode = mode === 'development';

    return {
        manifest_version: 2,
        name: 'VK Admin Tools 2' + (isDevMode ? ' [dev]' : ' [alpha]'),
        version: '1.0.6',
        permissions: [
            'https://vk.com/',
            isDevMode && 'file://*'
        ].filter(Boolean),
        background: {
            scripts: [
                'background.js'
            ]
        },
        content_scripts: [
            {
                matches: [
                    'https://vk.com/*',
                    isDevMode && 'file://*'
                ].filter(Boolean),
                js: [
                    'content-script.js'
                ]
            }
        ],
        content_security_policy: `script-src 'self' ${isDevMode ? '\'unsafe-eval\'' : '' }; object-src 'self'`
    };
};