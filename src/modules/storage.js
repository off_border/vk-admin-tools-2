const Vue = require('vue').default;
const actions = require('./actions')({});

module.exports = new Vue({
    data: {
        comments: [],
        filters: {
            userId: false,
            date: false
        },
        mainPanelOpened: false
    },

    computed: {
        visibleComments() {
            console.log('---visibleComments', this.filters);
            return this.comments.filter(comment => isCommentVisible(comment, this.filters));
        },

        hiddenComments() {
            return this.comments.filter(comment => !isCommentVisible(comment, this.filters));
        },

        commentsByUser() {
            const commentsData = extractByKey('_data', this.comments);

            return groupBy(commentsData, 'authorId', (key, comments) => ({
                key: comments[0].authorId,
                name: comments[0].authorName,
                thumb: comments[0].thumb,
                comments 
            }));
        },

        commentsByDate() {
            const commentsData = extractByKey('_data', this.comments);

            return groupBy(commentsData, 'baseDate', (key, comments) => ({
                key,
                name: key,
                comments 
            }));
        }
    },

    methods: {
        commit(event, payload) {
            console.log('---storage.commit', event, payload);

            switch (event) {
            
            case actions.UPDATE_COMMENTS:
                this.updateComments(payload);
                break;

            case actions.UPDATE_FILTERS:
                this.updateFilters(payload);
                break;

            case actions.OPEN_MAIN_PANEL:
                this.openMainPanel();
                break;

            case actions.TOGGLE_MAIN_PANEL:
                this.toggleMainPanel();
                break;


            }
        },

        updateComments(comments) {
            console.log('---storage.updateComments', comments);
            this.comments = comments;
            this.$emit('comments-updated');
        },

        updateFilters(newFilters) {
            console.log('---storage.updateFilters', newFilters, this.filters);
            this.toggleFilter(newFilters, 'userId');
            this.toggleFilter(newFilters, 'date');
            this.$emit('filters-updated');
        },

        toggleFilter(newFilters, filterName) {
            const oldFilter = this.filters[filterName];
            const newFilter = newFilters[filterName];

            if (newFilter) {
                if (!oldFilter) {
                    this.filters[filterName] = newFilter;
                } else {
                    if (oldFilter !== newFilter) {
                        this.filters[filterName] = newFilter;
                    } else {
                        this.filters[filterName] = false;
                    }
                }
            }
        },

        openMainPanel() {
            this.mainPanelOpened = true;
        },

        toggleMainPanel() {
            this.mainPanelOpened = !this.mainPanelOpened;
        }
    }
});

function isCommentVisible(comment, filters) {
    let visibleByUser = true;
    let visibleByDate = true;

    if (filters.userId) {
        if (comment._data.authorId !== filters.userId) {
            visibleByUser = false;
        }
    }

    if (filters.date) {
        if (comment._data.baseDate !== filters.date) {
            visibleByDate = false;
        }
    }

    return visibleByUser && visibleByDate;
}

function extractByKey(key, array) {
    return array.map(item => item[key]);
}

function groupBy(objArray, keyName, mapFn) {
    if (!mapFn) mapFn = (key, groupItems) => groupItems;

    const mappedByKey = objArray.reduce((result, item) => {
        const keyValue = item[keyName];
        if (!result[keyValue]) {
            result[keyValue] = [];
        }

        result[keyValue].push(item);

        return result;
    }, {});

    const keys = Object.keys(mappedByKey);

    return keys
        .map(key => {
            const groupItems = mappedByKey[key];
            return mapFn(key, groupItems);
        });
}
