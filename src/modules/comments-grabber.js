const storage = require('./storage');
const actions = require('./actions')(storage);
const getCommentHandler = require('./comment-handler/comment-handler');

const PageChangesListener = require('./page-changes-listener');
let pageChangesListener = new PageChangesListener();

module.exports = class CommentsGrabber {
    constructor() {
        this.rootDomNode = document.body;
        this.commentSelector = '.reply';
        this.comments = [];
        this._filters = {};

        storage.$on('filters-updated', this.applyFilters.bind(this));
        storage.$on('comments-updated', this.applyFilters.bind(this));
        pageChangesListener.listenToTheHref(this.onHrefChanged.bind(this));
    }

    getNotDeletedSelector() {
        return `${this.commentSelector}:not(${this.commentSelector}_deleted)`;
    }

    getCommentsNodes() {
        console.log('---CommentsGrabber.getComments()');
        // let comments = this.rootDomNode.querySelectorAll(this.getNotDeletedSelector());
        let comments = this.rootDomNode.querySelectorAll(this.commentSelector);
        return Array.from(comments);
    }
    
    updateComments() {
        console.log('---CommentsGrabber.updateComments()');
    
        let comments = this.getCommentsNodes()
            .map(getCommentHandler);
    
        console.log('--- CommentsGrabber.comments:', comments);

        storage.commit(actions.UPDATE_COMMENTS, comments);
    }

    setCommentSelector(selector) {
        this.commentSelector = selector;
    }

    listenToCommentHover(callback) {
        this._lastCommentNode = null;
        this.rootDomNode.addEventListener('mouseover', e => {
            let commentNode = e.target.closest(this.commentSelector);

            if (!commentNode) return;

            if (this._lastCommentNode !== commentNode) {
                this._lastCommentNode = commentNode;
                callback(commentNode);
            }
        });
    }

    applyFilters() {
        console.log('---CommentsGrabber.applyFilters', storage.filters, storage);
        storage.visibleComments.forEach(comment => comment.setHidden(false));
        storage.hiddenComments.forEach(comment => comment.setHidden(true));
    }

    onHrefChanged(href) {
        console.log('---onHrefChanged()');
    
        let isTopicPage = /\/topic-\d+/.test(href);
        let isWallPost = /\/wall-\d+/.test(href);
        let isWallPopup = isWallPost && /\?w=wall-\d+/.test(href);
    
        if (isTopicPage) {
            console.log('---onHrefChanged is topic page');
            this.setCommentSelector('.bp_post');
            pageChangesListener.listenToDomChanges('.bt_rows', () => this.updateComments());
    
        } 
        
        if (isWallPost) {
            if (isWallPopup) {
                console.log('---onHrefChanged is wall popup');
                this.setCommentSelector('.reply');
                pageChangesListener.listenToDomChanges('.wl_replies, .wl_replies .replies_list_deep', () => this.updateComments());
            } else {
                console.log('---onHrefChanged is wall post');
                this.setCommentSelector('.reply');
                pageChangesListener.listenToDomChanges('.replies_list, .replies_list_deep', () => this.updateComments());
            }
            
        }
    
        // setTimeout(() => this.updateComments(), 300);
    }
};