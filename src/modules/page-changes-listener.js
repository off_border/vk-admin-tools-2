module.exports = class PageChangesListener {
    constructor() {
        this.hrefListenerInterval = null;
        this.mutationObservers = [];
    }

    listenToTheHref(callback, checkingInterval = 100) {
        let oldHref = '';
    
        clearInterval(this.hrefListenerInterval);

        this.hrefListenerInterval = setInterval(() => {
            let newHref = document.location.href;
            if (newHref !== oldHref) {
                oldHref = newHref;
                callback(newHref);
            }
        }, checkingInterval);
    }

    listenToDomChanges(nodes, callback, { callbackOnFirstFound = true } = {}) {
        this.waitForNodes(nodes, nodes => {
            console.log('---listenToDomChanges: nodes found', nodes);
            if (this.mutationObservers.length) {
                this.mutationObservers.forEach(observer => observer.disconnect());
            }
    
            this.mutationObservers = Array.from(nodes)
                .map(node => {
                    let observer = new MutationObserver(callback);
                    observer.observe(node, { childList: true });
                    return observer;
                });

            callbackOnFirstFound && callback();
        });

        // if (typeof nodes === 'string') {
        //     nodes = document.querySelectorAll(nodes);
        // }

        // if (!nodes.length) throw new Error('DOM nodes should exist');

        // if (this.mutationObservers.length) {
        //     this.mutationObservers.forEach(observer => observer.disconnect());
        // }

        // this.mutationObservers = Array.from(nodes)
        //     .map(node => {
        //         let observer = new MutationObserver(callback);
        //         observer.observe(node, { childList: true });
        //         return observer;
        //     });
    }

    waitForNodes(nodes, callback) {
        const NODES_WAIT_RETRIES = 10;
        const NODES_WAIT_INTERVAL = 300;
        let retriesLeft = NODES_WAIT_RETRIES;

        clearInterval(this._nodesWaiterInerval);

        this._nodesWaiterInerval = setInterval(() => {
            let foundNodes;

            if (typeof nodes === 'string') {
                console.log('---searching for nodes', nodes);
                foundNodes = document.querySelectorAll(nodes);
            } else {
                console.log('--- waitForNodes: nodes already found');
                clearInterval(this._nodesWaiterInerval);
                return callback(nodes);
            }            

            if (foundNodes.length) {
                clearInterval(this._nodesWaiterInerval);
                return callback(foundNodes);
            }


            if (!retriesLeft) {
                clearInterval(this._nodesWaiterInerval);

                if (!foundNodes.length) 
                    throw new Error('DOM nodes should exist');
                else 
                    callback(foundNodes);
            }

            retriesLeft--;
        }, NODES_WAIT_INTERVAL);
    }
};