module.exports = class CommentHandlerTypeCommon {
    constructor(domElem) {
        this.domElem = domElem;

        this._thumbSelector = '.reply_image img';
        this._authorSelector = '.author';
        this._textSelector = '.reply_text';
        this._dateSelector = '.reply_date';
        this._controlsContainerSelector = '.reply_author';

        this.domElem._commentHandler = this;
    }

    getId() {
        return this.domElem.id;
    }

    getAuthorId() {
        return this.domElem.querySelector(this._authorSelector).dataset.fromId;
    }

    getThumb() {
        const thumbElem = this.domElem.querySelector(this._thumbSelector);

        if (!thumbElem) {
            console.log('---CommentHandler: thumb not found', this.domElem);
            return;
        }

        return thumbElem.getAttribute('src');
    }

    getAuthorName() {
        return this.domElem.querySelector(this._authorSelector).innerText;
    }

    getText() {
        return this.domElem.querySelector(this._textSelector).innerText;
    }

    getDate() {
        return this.domElem.querySelector(this._dateSelector).innerText;
    }

    getBaseDate() {
        const dateText = this.getDate();
        let baseDate;

        try {
            baseDate = /(\d+ [а-я]+( \d{4})?) в [\d:]+/.exec(dateText)[1];
        } catch(e) {
            try {
                baseDate =  /([а-я]+) в [\d:]+/.exec(dateText)[1];
            } catch(e) {
                try {
                    baseDate =  /\d+ ([а-я]+ [а-я]+)/.exec(dateText)[1];
                } catch(e) {
                    console.error('WRONG DATE REGEX');
                }
            }
            
        }

        return baseDate;
    }

    getData() {
        return {
            id: this.getId(),
            thumb: this.getThumb(),
            authorName: this.getAuthorName(),
            authorId: this.getAuthorId(),
            text: this.getText(),
            dateText: this.getDate(),
            baseDate: this.getBaseDate()
        };
    }

    updateData() {
        this._data = this.getData();
    }

    appendControlsNode(controlsElem) {
        console.log('---appendControlsNode');
        this.domElem.querySelector(this._controlsContainerSelector).appendChild(controlsElem);
    }

    hide() {
        this.domElem.style.display = 'none';
    }

    show() {
        this.domElem.style.display = '';
    }

    setHidden(hide) {
        if (hide) this.hide();
        else this.show();
    }
};
