const CommentHandlerTypeCommon = require('./comment-handler_type_common');
const CommentHandlerTypeCommonDeleted = require('./comment-handler_type_common-deleted');
const CommentHandlerTypeTopic = require('./comment-handler_type_topic');
const CommentHandlerTypeTopicDeleted = require('./comment-handler_type_topic-deleted');

module.exports = function getCommentHandler(commentNode) {
    let commentExtractor = getCommentHandlerBySelector(commentNode);
    
    if (commentExtractor) {
        commentExtractor.updateData();
        return commentExtractor;
    } else {
        console.log('---getCommentHandler() ERROR: unsupported comment type');
    }
};

function getCommentHandlerBySelector(commentNode) {
    if (commentNode.matches('.reply')) {
        if (commentNode.matches('.reply_deleted'))
            return new CommentHandlerTypeCommonDeleted(commentNode);
        return new CommentHandlerTypeCommon(commentNode);
    }
    if (commentNode.matches('.bp_post')) {
        if (commentNode.matches('.bp_post_deleted'))
            return new CommentHandlerTypeTopicDeleted(commentNode);
        return new CommentHandlerTypeTopic(commentNode);
    }
}