const CommentHandlerTypeCommon = require('./comment-handler_type_common');

module.exports = class CommentHandlerTypeTopic extends CommentHandlerTypeCommon {
    constructor(domElem) {
        super(domElem);

        this._thumbSelector = '.bp_thumb img';
        this._authorSelector = '.bp_author';
        this._textSelector = '.bp_text';
        this._controlsContainerSelector = '.bp_author_wrap';
        this._dateSelector = '.bp_date';
    }

    getAuthorId() {
        return this.domElem.querySelector(this._authorSelector).getAttribute('href').replace('/', '');
    }
};
