const CommentHandlerTypeCommon = require('./comment-handler_type_common');

module.exports = class CommentHandlerTypeCommonDeleted extends CommentHandlerTypeCommon {
    getAuthorId() {}
    getThumb() {}
    getAuthorName() {}
    getText() {}

    getData() {
        return {
            ...super.getData(),
            isDeleted: true
        };
    }

    appendControlsNode() {
        console.log('---appendControlsNode: node is deleted');
    }
};
