const Vue = require('vue').default; 
const CommentControls = require('./modules/comment-controls.vue').default;
const CommentsGrabber = require('./modules/comments-grabber');
const MainPanel = require('./modules/main-panel/main-panel.vue').default;

let commentsGrabber = new CommentsGrabber();

let commentControls = new Vue(CommentControls);
commentControls.$mount();

commentsGrabber.listenToCommentHover(appendCommentControls);

let mainPanel = new Vue(MainPanel);
mainPanel.$mount();
document.body.appendChild(mainPanel.$refs.domEl);

function appendCommentControls(commentNode) {
    if (!commentNode._commentHandler) return;
    commentNode._commentHandler.appendControlsNode(commentControls.$refs.domEl);
    commentControls.handler = commentNode._commentHandler;
}

window.commentsGrabber = commentsGrabber;
