/* global __dirname */

const path = require('path');
const ChromeExtensionReloader = require('webpack-chrome-extension-reloader');
const VueLoaderPlugin = require('vue-loader/lib/plugin');
const WriteJSONPlugin = require('write-json-webpack-plugin');
const ZipPlugin = require('zip-webpack-plugin');



module.exports = (env, argv) => {

    const mode = argv.mode || 'development';
    const isDevMode = mode === 'development';
    const isProdMode = !isDevMode;

    return {
        mode,

        output: {
            path: path.resolve(__dirname, isDevMode ? 'dist-dev' : 'dist')
        },

        entry: {
            background: './src/background.js',
            'content-script': './src/content-script.js'
        },

        module: {
            rules: [
                {
                    test: /\.vue$/,
                    loader: 'vue-loader'
                },
                {
                    test: /\.styl(us)?$/,
                    use: [
                        'vue-style-loader',
                        'css-loader',
                        'stylus-loader'
                    ]
                },
                {
                    test: /\.css$/,
                    loader: [
                        'vue-style-loader',
                        'css-loader'
                    ]
                }
            ]
        },

        plugins: [
            isDevMode &&
            new ChromeExtensionReloader(),

            new VueLoaderPlugin(),

            new WriteJSONPlugin({
                filename: 'manifest.json',
                object: require('./src/manifest')({ mode }),
                pretty: true
            }),

            isProdMode &&
            new ZipPlugin({
                path: 'build',
                filename: 'build.zip'
            })
        ].filter(Boolean)

    };
}; 